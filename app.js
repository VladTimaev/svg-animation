const url = "file:///home/vladislav/Desktop/Belarus-company/register.html?"
const alertPlaceholder = document.getElementById('liveAlertPlaceholder');
const button = document.querySelector(".btn");


// Validatio Form


const usernameEl = document.querySelector('#username');
const emailEl = document.querySelector('#email');
const passwordEl = document.querySelector('#password');
const confirmPasswordEl = document.querySelector('#confirm-password');
const form = document.querySelector('#signup');
const secondEl = document.querySelector('#secondname');


const alert = (message, type) => {
  const wrapper = document.createElement('div')
  wrapper.innerHTML = [
    `<div class="alert alert-${type} alert-dismissible" role="alert">`,
    `   <div>${message}</div>`,
    '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
    '</div>'
  ].join('')
  
  alertPlaceholder.append(wrapper)
}

const checkUsername = () => {

    let valid = false;

    const min = 8,
          max = 25;

    const username = usernameEl.value.trim();
 
    if (!isRequired(username)) {
       showError(usernameEl, 'Username cannot be blank.');
    } else if (!isBetween(username.length, min, max)) {
       showError(usernameEl, `Username must be between ${min} and ${max} characters.`)
    } else {
        showSuccess(usernameEl);
        valid = true;
    }
    return valid;
};

const checkSecondname = () => {
  let valid = false;
   const min = 8,
        max = 25;

  const secondName = secondEl.value.trim();

  if (!isRequired(secondName)) {
      showError(secondEl, 'Username cannot be blank.');
  } else if (!isBetween(username.length, min, max)) {
      showError(secondEl, `Username must be between ${min} and ${max} characters.`)
  } else {
      showSuccess(secondEl);
      valid = true;
  }
  return valid;
};



const checkEmail = () => {
    let valid = false;
    const email = emailEl.value.trim();
    if (!isRequired(email)) {
        showError(emailEl, 'Email cannot be blank.');
    } else if (!isEmailValid(email)) {
        showError(emailEl, 'Email is not valid.')
    } else {
        showSuccess(emailEl);
        valid = true;
    }
    return valid;
};

const addAnimation = () => { 
  const button = document.getElementById("liveAlertBtn");
    button.addEventListener("click", () => {
    button.classList.add("slidein_btn")
 })
}

const removeAnimation = () => { 
  const button = document.getElementById("liveAlertBtn");
    button.addEventListener("click", () => {
    button.classList.remove("slidein_btn")
 })
}


const checkPassword = () => {
    let valid = false;
    const password = passwordEl.value.trim();
    addAnimation();
    if (!isRequired(password)) {
        showError(passwordEl, 'Password cannot be blank.');
    } else if (!isPasswordSecure(password)) {
        showError(passwordEl, 'Password must has at least 8 characters that include at least 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character in (!@#$%^&*)');
    } else {
        showSuccess(passwordEl);
        valid = true;
    }
    return valid;
};

const checkConfirmPassword = () => {
    let valid = false;
    // check confirm password
    const confirmPassword = confirmPasswordEl.value.trim();
    const password = passwordEl.value.trim();
    addAnimation();
    if (!isRequired(confirmPassword)) {
    } else if (password !== confirmPassword) {
        showError(confirmPasswordEl, 'The password does not match');
    } else {
        alert("Регистрация прошла успешно", "success")
        setTimeout(function(){
          window.location.href =  url;
        }, 2000); 
        valid = true;
    }

    return valid;
};

const isEmailValid = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

const isPasswordSecure = (password) => {
    const re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    return re.test(password);
};

const isRequired = value => value === '' ? false : true;
const isBetween = (length, min, max) => length < min || length > max ? false : true;






const showError = (input, message) => {
    // get the form-field element
    const formField = input.parentElement;
    // add the error class
    formField.classList.remove('form-succes');
    formField.classList.add('form-error');
    // show the error message
    const error = formField.querySelector('small');
    error.textContent = message;
};

const showSuccess = (input, message) => {
    // get the form-field element
    const formField = input.parentElement;

    // remove the error class
    formField.classList.remove('form-error');
    formField.classList.add('form-succes');

    // hide the error message
    const error = formField.querySelector('small');
    error.textContent = message;
}


form.addEventListener('submit', function (e) {
    // prevent the form from submitting
    e.preventDefault();

    // validate fields
    let isUsernameValid = checkUsername(),
        isSurnameValid = checkSecondname(),
        isEmailValid = checkEmail(),
        isPasswordValid = checkPassword(),
        isConfirmPasswordValid = checkConfirmPassword();

    let isFormValid = isUsernameValid && 
        isSurnameValid &&
        isEmailValid &&
        isPasswordValid &&
        isConfirmPasswordValid;

    // submit to the server if the form is valid
    if (isFormValid) {     
    }
});


const debounce = (fn, delay = 500) => {
    let timeoutId;
    return (...args) => {
        // cancel the previous timer
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
        // setup a new timer
        timeoutId = setTimeout(() => {
            fn.apply(null, args)
        }, delay);
    };
};

form.addEventListener('input', debounce(function (e) {
    switch (e.target.id) {
        case 'username':
            checkUsername();
            break;
        case 'secondname':
            checkSecondname();
            break;
        case 'email':
            checkEmail();
            break;
        case 'password':
            checkPassword();
            break;
        case 'confirm-password':
            checkConfirmPassword();
            break;
    }
}));


